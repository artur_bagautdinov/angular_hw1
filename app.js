// тайпскирпт - программа по отрисовке фигур на канвасе, 
// типа пользователь вводит координаты точек ( или кликает) 
// а оно ему рисует фигуру и подсчитывает его площадь и периметр
(function () {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext("2d");
    var isFigureCompleted = false;
    var minCordsToCloseFigure = 5;
    var pointRadius = 5;
    var perimeters = [];
    var figureCount = 1;
    var currentFigurePerimeter = 0;
    var isStartCords = false;
    var startPointCords = {
        x: 0,
        y: 0
    };
    var mouseCords = {
        x: 0,
        y: 0
    };
    var getLineSize = function (prevX, prevY, currentX, currentY) {
        var x = Math.abs(prevX - currentX);
        var y = Math.abs(prevY - currentY);
        if (x - y > 0) {
            return x;
        }
        return y;
    };
    var draw = function (e) {
        if (!startPointCords.x && !startPointCords.y) {
            isStartCords = true;
        }
        var prevX = mouseCords.x;
        var prevY = mouseCords.y;
        mouseCords.x = e.pageX - canvas.offsetLeft;
        mouseCords.y = e.pageY - canvas.offsetTop;
        if (isStartCords) {
            prevX = mouseCords.x;
            prevY = mouseCords.y;
            startPointCords.x = mouseCords.x;
            startPointCords.y = mouseCords.y;
        }
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(mouseCords.x, mouseCords.y);
        if (isStartCords) {
            isStartCords = false;
            ctx.arc(mouseCords.x, mouseCords.y, pointRadius, 0, 2 * Math.PI);
            ctx.stroke();
        }
        else {
            var diffCordsX = Math.abs(startPointCords.x - mouseCords.x);
            var diffCordsY = Math.abs(startPointCords.y - mouseCords.y);
            var linesize = getLineSize(prevX, prevY, mouseCords.x, mouseCords.y);
            currentFigurePerimeter += linesize;
            if (diffCordsX <= minCordsToCloseFigure && diffCordsY <= minCordsToCloseFigure) {
                ctx.lineTo(startPointCords.x, startPointCords.y);
                ctx.stroke();
                startPointCords.x = 0;
                startPointCords.y = 0;
                var figure = {};
                perimeters.push({ name: "figure " + figureCount, perimeter: currentFigurePerimeter });
                figureCount++;
                setTimeout(function () {
                    alert("Current perimeter is : " + currentFigurePerimeter);
                    currentFigurePerimeter = 0;
                }, 1000);
            }
            else {
                ctx.arc(mouseCords.x, mouseCords.y, pointRadius, 0, 2 * Math.PI);
                ctx.stroke();
            }
        }
    };
    canvas.addEventListener('click', draw);
})();
