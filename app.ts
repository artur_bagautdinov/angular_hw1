// тайпскирпт - программа по отрисовке фигур на канвасе, 
// типа пользователь вводит координаты точек ( или кликает) 
// а оно ему рисует фигуру и подсчитывает его площадь и периметр

(function () {

    const canvas = <HTMLCanvasElement>document.getElementById('canvas');
    const ctx = canvas.getContext("2d");
    let isFigureCompleted: boolean = false;
    const minCordsToCloseFigure = 5;
    const pointRadius: number = 5;
    const perimeters: Perimeters[] = [];
    let figureCount:number = 1;
    let currentFigurePerimeter: number = 0;

    let isStartCords = false;

    interface Perimeters   {
        name: string,
        perimeter: number
    }

    interface Cords {
        x: number,
        y: number
    }

    const startPointCords: Cords = {
        x: 0,
        y: 0
    }

    const mouseCords: Cords = {
        x: 0,
        y: 0
    }

    const getLineSize = (prevX:number, prevY:number, currentX:number, currentY:number): number => {
        const x = Math.abs(prevX - currentX);
        const y = Math.abs(prevY - currentY);

        if (x - y > 0) {
            return x;
        }

        return y;
    }

    const draw = (e: MouseEvent): void => {

        if (!startPointCords.x && !startPointCords.y) {
            isStartCords = true;
        }

        let prevX = mouseCords.x;
        let prevY = mouseCords.y;

        mouseCords.x = e.pageX - canvas.offsetLeft;
        mouseCords.y = e.pageY - canvas.offsetTop;

        if (isStartCords) {

            prevX = mouseCords.x;
            prevY = mouseCords.y;

            startPointCords.x = mouseCords.x;
            startPointCords.y = mouseCords.y;
        }

        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(mouseCords.x, mouseCords.y);




        if (isStartCords) {
            isStartCords = false;
            ctx.arc(mouseCords.x, mouseCords.y, pointRadius, 0, 2 * Math.PI);
            ctx.stroke();
        } else {
            const diffCordsX: number = Math.abs(startPointCords.x - mouseCords.x);
            const diffCordsY: number = Math.abs(startPointCords.y - mouseCords.y);

            const linesize = getLineSize(prevX, prevY, mouseCords.x, mouseCords.y);
            currentFigurePerimeter += linesize;

            if (diffCordsX <= minCordsToCloseFigure && diffCordsY <= minCordsToCloseFigure) {

                ctx.lineTo(startPointCords.x, startPointCords.y);
                ctx.stroke();

                startPointCords.x = 0;
                startPointCords.y = 0;

                const figure = {}

                perimeters.push({name: `figure ${figureCount}`, perimeter : currentFigurePerimeter});
                figureCount++;
                
                setTimeout(() : void=>{
                    alert(`Current perimeter is : ${currentFigurePerimeter}`);
                    currentFigurePerimeter = 0;
               }, 1000)

                
            } else {
                ctx.arc(mouseCords.x, mouseCords.y, pointRadius, 0, 2 * Math.PI);
                ctx.stroke();
            }

        }

    }


    canvas.addEventListener('click', draw)


})()